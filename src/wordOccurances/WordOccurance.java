package wordOccurances;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class WordOccurance {

	public static void main(String[] args) throws Exception {
		String line,word = "";
		int count, maxCount = 0 ;
		ArrayList<String> words = new ArrayList<String>();
		FileReader file = new FileReader("/Users/Meghna/Downloads/Coding skills test/islands_in_the_stream_client.txt");
		BufferedReader br = new BufferedReader(file);
		while((line = br.readLine()) != null) {    
            String string[] = line.toLowerCase().split("([,.\\s]+) ");    
            //Adding all words generated in previous step into words    
            for(String s : string){    
                words.add(s);    
            }    
        }    
            
        for(int i = 0; i < words.size(); i++){    
            count = 1;    
            for(int j = i+1; j < words.size(); j++){    
                if(words.get(i).equals(words.get(j))){    
                    count++;    
                }     
            }    
            
            if(count > maxCount){    
                maxCount = count;    
                word = words.get(i);    
            }    
        }    
            
        System.out.println("Most repeated word: " + word);    
        br.close();    
    }    
}    
